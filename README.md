# A11y Playground

The purpose of this project is to create a playground to demo a11y techniques / tools.  

The JS project is currently React-based and was was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) & [CRACO](https://github.com/gsoft-inc/craco/blob/master/packages/craco).  It contains code from deque university.  

The PHP project is based on [Accessible University] (https://github.com/terrill/AU).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the react app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn php`

Runs the php app with the inbuilt php server.  Visit [http://localhost:8000/index.php](http://localhost:8000/index.php) to see it in action.

### `yarn test`

Launches the Jest test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn cypress:open`

Launches the Cypress test runner.

### `yarn build`

Builds the react app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
