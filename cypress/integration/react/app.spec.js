describe('Testing App.tsx', function() {
  it('Contains no a11y problems on load', function() {
    cy.visit('http://localhost:3000');
    cy.injectAxe();

    cy.checkA11y();
  })
});
