describe('Testing AU Homepage', function() {
  it('Contains no a11y problems on load', function() {
    cy.visit('http://localhost:8042/before.html');
    cy.injectAxe();

    cy.checkA11y();
  })
});
