import { axe, toHaveNoViolations } from 'jest-axe';
import React from 'react';
import { render, cleanup } from '@testing-library/react';
import App from './App';

afterEach(cleanup);

test('renders learn react link', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/learn react/i);

  expect(linkElement).toBeInTheDocument();
});

test('has no a11y mistakes', async () => {
  expect.extend(toHaveNoViolations);

  const { container } = render(<App />);
  const results = await axe(container);

  expect(results).toHaveNoViolations();
});
